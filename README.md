# Introduction

This is quick and dirty mash-up of two projects to interface the RFID-RC522 module with BusPirate interface (no other electronics involved). The code is not pretty, but it works and can be used as reference implementation.

The first project is [pyBusPirateLite](https://code.google.com/p/the-bus-pirate/downloads/detail?name=pyBusPirateLite-r597.zip), a python 2.7 library for controlling the BusPirate in binary mode. It is included in repository; the only modifications I made was removal of several timeouts.

The second project is [MFRC522-python](https://github.com/mxgxw/MFRC522-python); written in C-like python 2.7 it implements the SPI communication for this RFID chip on RPi platform. The code is modified to work with BusPirate SPI interface instead of RPi's built-in SPI bus.

# Hardware

Only RFID-RC522 module and BusPirate is needed. Connection is straightforward, these signals are directly connected between boards: CS, CLK, MOSI, MISO, GND and 3V3. 

Reset is not used, a soft-reset is triggered by SPI command. BusPirate's VPU pin does not need to be connected as pull-ups are not used.

When BusPirate is plugged into USB port, it will register itself as a virtual COM port. The rfid_rc522.py needs to be modified with a correct port name.

# Scripts

*rfid_rc522.py* implements SPI communication, to be used from other scripts

*rfid_dump.py* waits for nearby card, then reads and prints any information stored on card

*rfid_speak.py* learns and speaks the names of different cards (relies on espeak utility)